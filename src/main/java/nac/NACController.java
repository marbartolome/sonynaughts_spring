package nac;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import nac.Game.BoardPos;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class NACController {
    // Using this instead of a database.
	private final Map<Long, Game> gameInstances = new HashMap<Long,Game>();
	// And this for generating game ids
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/startgame")
    public @ResponseBody Game startGame() {
    	long gameId = counter.incrementAndGet();
    	Game newGame = new Game(gameId);
        gameInstances.put(gameId, newGame);
        return newGame;
    }
    
    @RequestMapping("/move_x")
    public @ResponseBody Game moveX(
    		@RequestParam(value="row", required=true) int row,
    		@RequestParam(value="col", required=true) int col,
    		@RequestParam(value="gameid", required=true) long gameId
    		) {
    	return move(gameId, CellContent.X, col, row);
    }
    
    @RequestMapping("/move_o")
    public @ResponseBody Game moveO(
    		@RequestParam(value="row", required=true) int row,
    		@RequestParam(value="col", required=true) int col,
    		@RequestParam(value="gameid", required=true) long gameId
    		) {
    	return move(gameId, CellContent.O, col, row);
    }
    
    public Game move(Long gameId, CellContent movingSymbol, int col, int row){
    	try{
    		Game game = getGame(gameId);
    		if(!game.hasGameEnded() && game.player.playerSymbol.equals(movingSymbol)){
    			return getGame(gameId).move(movingSymbol, new BoardPos(col,row));
    		} else {
    			throw new ForbiddenRequestException();
    		}
    	} catch (IllegalArgumentException e){
    		throw new ForbiddenRequestException();
    	}
    }
    
    @ResponseStatus(value = org.springframework.http.HttpStatus.NOT_FOUND)
    public final class ResourceNotFoundException extends RuntimeException {
    }
    @ResponseStatus(value = org.springframework.http.HttpStatus.PRECONDITION_FAILED)
    public final class ForbiddenRequestException extends RuntimeException {
    }
    
    public Game getGame(long gameId){
    	if (gameInstances.containsKey(gameId)){
	    	Game game = gameInstances.get(gameId);
	        return game;
    	} else{
	        throw new ResourceNotFoundException();
    	}
    }
    
    @RequestMapping("/checkstate")
    public @ResponseBody Game checkState(
    		@RequestParam(value="gameid", required=true) long gameId
    		) {
    	return getGame(gameId);
    }
}
