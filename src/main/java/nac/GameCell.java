package nac;

import nac.Game.BoardPos;

public class GameCell {
	public CellContent content;
	public BoardPos pos;
	
	public GameCell(BoardPos pos){
		this.content = CellContent.EMPTY;
		this.pos = pos;
	}
	
	public GameCell setContent(CellContent content){
		if (this.content!=CellContent.EMPTY){
			throw new IllegalArgumentException("Cell already in use");
		} else {
			this.content = content;
		}
		return this;
	}
	
	public String toString(){
		return content.toString();
	}
}
