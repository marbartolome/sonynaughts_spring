package nac;

public enum CellContent {
	O("O"), X("X"), EMPTY("·");
	
	String strRep;
	
	private CellContent(String str){
		this.strRep = str;
	}
	
	public String toString(){
		return strRep;
	}
}
