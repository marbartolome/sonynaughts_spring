package nac;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;

public class Game {
	public static class BoardPos {
		public BoardPos(int x, int y){
			this.x = x;
			this.y = y;
		}
		public int x;
		public int y;
	}
	public static enum PlayerTurn {
		X(CellContent.X), O(CellContent.O);
		
		public CellContent playerSymbol;
		
		private PlayerTurn(CellContent symbol){
			playerSymbol = symbol;
		}
		
		public String toString(){
			return "Player-"+playerSymbol;
		}
		
		public PlayerTurn nextPlayer(){
			if (this.equals(X)) return O;
			else return X;
		}
	}
	public GameCell[][] gameBoard = new GameCell[3][3];
	public PlayerTurn player = PlayerTurn.X;
	public CellContent winner = CellContent.EMPTY;
	public long id;
	public boolean gameFinished = false;
	
	public Game(){
		for (int x=0; x<3; x++){
			for(int y=0;y<3; y++){
				BoardPos pos = new BoardPos(x, y);
				gameBoard[y][x] = new GameCell(pos);
			}
		}
	}
	
	public Game(long gameId){
		this();
		id = gameId;
	}
	
	public Game move(CellContent content, BoardPos pos){
		try{
			gameBoard[pos.y][pos.x].setContent(content);
			if (check3InARow(pos)){
				gameFinished = true;
				winner = content;
			} else if (checkDraw()){
				gameFinished = true;
			} else {
				player = player.nextPlayer();
			}
		} catch (IllegalArgumentException e){
			throw new IllegalArgumentException("Can't move there, cell in use");
		} catch (ArrayIndexOutOfBoundsException e){
			throw new IllegalArgumentException("Can't move there, valid positions are only 1 2 or 3");
		}
		return this;
	}
	
	
	public GameCell get(BoardPos pos){
		return gameBoard[pos.y][pos.x];
	}
	
	/**
	 * Check if the contents 3 given board positions are equal
	 * (but different from {@link CellContent.EMPTY}!)
	 * @param cell1
	 * @param cell2
	 * @param cell3
	 * @return
	 */
	public boolean check3Equals(BoardPos cell1, BoardPos cell2, BoardPos cell3){
		return get(cell1).content.equals(get(cell2).content) && get(cell1).content.equals(get(cell3).content)
				&& !get(cell1).content.equals(CellContent.EMPTY);
	}
	
	public boolean checkDraw(){
		for (GameCell[] row : gameBoard){
			for (GameCell cell : row){
				if (cell.content.equals(CellContent.EMPTY)) return false;
			}
		}
		return true;
	}
	
	 
	/**
	 * Checks if there's 3 cells of the same type in 
	 * the same vertical, horizontal or diagonal 
	 * from the given cell.
	 * @param lastPlacedPos
	 * @return
	 */
	public boolean check3InARow(BoardPos lastPos){
		
		return (
			// Horizontal check
			(check3Equals(new BoardPos(0, lastPos.y), new BoardPos(1, lastPos.y), new BoardPos(2, lastPos.y) ))
		|| // vertical check
		    (check3Equals(new BoardPos(lastPos.x, 0), new BoardPos(lastPos.x, 1), new BoardPos(lastPos.x, 2) ))
		|| // diagonal 1 check
			(check3Equals(new BoardPos(0,0), new BoardPos(1,1), new BoardPos(2,2) ))
		|| // diagonal 2 check
			(check3Equals(new BoardPos(0,2), new BoardPos(1,1), new BoardPos(2,0) )) );
	}
	
	public boolean hasGameEnded(){
		return gameFinished;
	}
	
	public String toString(){
		String rep = "";
		for (GameCell[] row : gameBoard){
			for (GameCell cell : row){
				rep+=cell;
			}
			rep+="\n";
		}
		return rep;
	}
	
	
	public static BoardPos parseInput(String userInput){
		
		String[] inPos = userInput.split(",");
		if (inPos.length<2){
			throw new IllegalArgumentException("Error: Invalid format, should be like '1,3'");
		}
		try {
			int x = Integer.parseInt(inPos[1]);
			int y = Integer.parseInt(inPos[0]);
			if (x>0 && x<4 && y>0 && y<4){
				return new BoardPos(x-1, y-1);
			} else {
				throw new IllegalArgumentException("Error: Invalid format, positions should be between 1 and 3");
			}
		} catch (NumberFormatException e){
			throw new IllegalArgumentException("Error: Invalid format, should be like '1,3'");
		}
		
	}
	
	public static void main(String[] args){
		Game game = new Game();
		Console cns = System.console();
		
		try {
			BufferedReader bReader = new BufferedReader(new InputStreamReader(System.in));
			
			while (true){
				System.out.println(game.toString());
				System.out.println(game.player.toString()+" turn");
				// Payer movement; retry until successful
				boolean moveOk = false; 
				while(!moveOk){
					System.out.println(">> Enter comma separated position like 'row,column' (ex: 1,3): ");
					try{

						BoardPos pos = parseInput(bReader.readLine());
						try{
							game.move(game.player.playerSymbol, pos);
							moveOk = true;
						} catch (IllegalArgumentException e){
							System.out.println("Can't move there: "+e.getMessage());
						}
						
					} catch(IllegalArgumentException e){
						System.out.println(e.getMessage());
					}
					if (!moveOk){
						System.out.println(game.player.toString()+" try again");
					}
				}
				
				// check game end
				if (game.hasGameEnded()){
					if (game.winner!=CellContent.EMPTY){
						System.out.println(game.player+" wins!");
					} else {
						System.out.println("Draw - nobody wins");
					}
					break;
				}
			}
		} catch (IOException e){
			System.exit(1);
		}

		System.exit(0);
	}
}
